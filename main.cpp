#include <iostream> // потоковый ввод и вывод
#include <locale.h> // для языка
#include <iomanip> //для форматного вывода
#include <string>
#include <cstring>
#include <fstream>

using namespace std;


struct Matrix { //создаем структуру, хранящую в себе данные о:кол-ве строк(m), количестве столбцов(n), и элементах матрицы(matrix)
	float** matrix;
	int m;
	int n;
};



void create_matrix(Matrix* A, int m, int n) { //создание матрицы
	A->m = m; //отдаём значение
	A->n = n;
	A->matrix = new float*[A->m];
	for (int i = 0; i < A->m; i++) {
		(A->matrix)[i] = new float[A->n]{0};
	}
}


void destroy_matrix(Matrix* A) { //удаление мтрицы
	for (int i = 0; i < A->m; i++)
		delete[] A->matrix[i];//устраняем утечку памяти, стирая динамический массив
	delete[] A->matrix;
}


void print_matrix(Matrix* A) { //функция печати матрицы // A->n - передаём значение
	for (int i = 0; i < A->m; i++) {
		for (int j = 0; j < A->n; j++) 
			cout << setw(4) << A->matrix[i][j];
		cout << endl;
	}
}

Matrix* matrix_trans(Matrix* A) { // транспонирование матрицы
	Matrix* B = new Matrix;
	create_matrix(B, A->m, A->n);

	for (int i = 0; i < A->m; i++) {
		for (int j = 0; j < A->n; j++)
			B->matrix[j][i] = A->matrix[i][j];
	}
	return B;
}

Matrix* matrix_minor(Matrix* A, int i, int j) {
	Matrix* B = new Matrix;
	create_matrix(B, A->n - 1, A->n - 1);
	// минор матрицы
	for (int o = 0, k = 0; o < A->m; o++) {
		if (o == i) {                                     //k, g - новые счетчики в новой матрице, миноре
			continue;
		}
		for (int p = 0, g = 0; p < A->n; p++) { // вычеркиваем строку и столбец
			if (p == j) {
				continue;
			}
			B->matrix[k][g++] = A->matrix[o][p];
		}
		k++;
	}
	return B;
}

float determinant(Matrix* A) {
	if (A->n == 1) //определитель матрицы, состоящей из 1 числа равен этому числу
		return A->matrix[0][0];

	int res = 0;
	//раскладываем по 0 строке
	for (int i = 0; i < A->n; i++) {
		Matrix* B = matrix_minor(A, 0, i);
		res = res + ((i % 2) ? (-1) : 1) * (A->matrix[0][i]) * determinant(B); //прибавляем к предрезу (-1)^(i)*'элемент нулевой строки*определитель минора
		destroy_matrix(B);
		delete B;
	}
	return res;
}


Matrix* inverted_matrix(Matrix* A) { // нахождение обратной матрицы
	int det = determinant(A);

	Matrix* ans = new Matrix;
	create_matrix(ans, A->m, A->n);
	for (int i = 0; i < A->m; ++i) {
		for (int j = 0; j < A->n; ++j) {
			Matrix* B = matrix_minor(A, i, j);
			ans->matrix[i][j] = ((i + j) % 2 ? -1 : 1) * determinant(B); // умножаем (-1)^(i+j)*определитель минора
			destroy_matrix(B);
			delete B;
		}//повторяем , у меньшая порядок минора
	}
	Matrix* ans_t = matrix_trans(ans); //транспонируем итоговую матрицу дополнений
	destroy_matrix(ans);
	delete ans; 
	for (int i = 0; i < ans_t->m; i++) {
		for (int j = 0; j < ans_t->n; j++) {
			ans_t->matrix[i][j] /= det;
		}
	}
	return ans_t;
}


void matrix_input(Matrix* A) { //введение матрицы с клавиатуры
	cout << "Введите матрицу: " << A->m << "x" << A->n << endl;

	for (int i = 0; i < A->m; i++) {
		for (int j = 0; j < A->n; j++) {
			cin >> A->matrix[i][j];
		}
	}
}

Matrix* matrix_sum(Matrix* A, Matrix* B) { //сумма матриц
	if (A->m != B->m || A->n != B->n) {
		return NULL;
	}

	Matrix* C = new Matrix;
	create_matrix(C, A->m, A->n);
	for (int i = 0; i < A->m; i++) {
		for (int j = 0; j < A->n; j++) {
			C->matrix[i][j] = B->matrix[i][j] + A->matrix[i][j];
		}
	}
	return C;
}


// не забыть удалить матрицу
Matrix* multiplication(Matrix* A, Matrix* B) { //умножение матриц
	Matrix* C = new Matrix;
	create_matrix(C, A->m, B->n);

	for (int i = 0; i < A->m; i++)
		for (int j = 0; j < B->n; j++)
			for (int z = 0; z < A->n; z++)
				C->matrix[i][j] += A->matrix[i][z] * B->matrix[z][j];

	return C;
}


// stepen - положительное число
// n - размерность матрицы
// A - матрица
Matrix* pow(Matrix* A, int stepen) { //возведение в степень
	Matrix* C = new Matrix;
	create_matrix(C, A->m, A->n);
	for (int i = 0; i < C->m; i++) {
		C->matrix[i][i] = 1;
	}

	for (int u = 0; u < stepen; u++) {
		Matrix* D = multiplication(A, C);
		for (int i = 0; i < D->m; i++) {
			for (int j = 0; j < D->n; j++) {
				C->matrix[i][j] = D->matrix[i][j];
			}
		}
		destroy_matrix(D);
		delete D;
	}

	return C;
}


int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "RUS");
	Matrix A;
	int vivod = 2;
	int l = 2;
	if (argc >= 2) {
		create_matrix(&A, atoi(argv[1]), atoi(strchr(argv[1], 'x') + 1)); //strchr - стрингчар, поиск элемента в строке и возвращение его позиции
		if (argc > 2) {
			char* b = argv[2];
			int i = 0;
			while (true) {
				(A.matrix)[i / A.n][i % A.n] = atoi(b);
				i++;
				char* c = strchr(b, ',');
				if (c == 0)
					break;
				b = c + 1;
			}
		}

		for (int i = 0; i < A.m; i++) {
			for (int j = 0; j < A.n && l < argc; j++, l++) {
				(A.matrix)[i][j] = atoi(argv[l]);
			}
		}
	}

	if (A.m == 0 || A.n == 0 || argc == 1)
		vivod = 1;

	do {
		int sw;
		cout << "01.Вывести матрицу\n" "02.Сложить матрицу\n" "03.Умножить матрицу\n" "04.Транспонировать матрицу\n" "05.Расширить матрицу\n" "06.Найти элементы\n"
			"07.Изменить значение элемента\n" "08.Возвести в степень\n" "09.Вычислить определитель матрицы\n"
			"10.Вычислить обратную матрицу\n" "11.Вычислить многочлен матрицы\n" "12.Сохранить в файл\n" "13.Загрузить из файла\n"
			"14.Сортировать матрицу\n" "15.Выйти из программы\n";
		cin >> sw;
		if (vivod == 1 && sw != 15) {
			cout << "Матрица пустая\n";
			cout << endl;
			continue;
		}
		switch (sw) {
		case 1:
			print_matrix(&A);
			break;
		case 2: {
			Matrix B; //функция принимает указатель на матрицу, поэтому передаем ей указатель
			create_matrix(&B, A.m, A.n);
			matrix_input(&B);
			Matrix* C = matrix_sum(&A, &B);
			print_matrix(C);
			destroy_matrix(&B);
			destroy_matrix(C);
			delete C;
			break;
		}
		case 3: {
			int h = 0, g = 0;
			cout << "Введите размер матрицы:" << endl;
			char ch;
			cin >> g >> ch >> h;

			if (h == 0 || g == 0 || g != A.n) {
				cout << "Неверный размер" << endl;
				break;
			}

			Matrix B;
			create_matrix(&B, g, h);
			matrix_input(&B);
			
			Matrix* C = multiplication(&A, &B);
			cout << "Результат:" << endl;
			print_matrix(C);
			destroy_matrix(C);
			delete C;
			destroy_matrix(&B);
			break;
		}
		case 4: {
			Matrix* B = matrix_trans(&A);
			destroy_matrix(&A);
			A = *B;
			delete B;
			break;
		}
		case 5: {
			cout << "Выберите направление:" << endl << "h ►" << endl << "k ↑" << endl << "l ←" << endl << "j ↓" << endl;
			
			char naprav;
			cin >> naprav;

			if (naprav == 'h') {
				A.n++;
				for (int i = 0; i < A.m; i++) {
					A.matrix[i] = (float*) realloc(A.matrix[i], A.n * sizeof(float*)); //realloc - выгоднее нью, потому что может либо расширить область памяти, либо, если не получилось расширить,создает новую ячейку большего размера, записывая туда старые значения
					A.matrix[i][A.n - 1] = 0;
				}
			}
			if (naprav == 'k') {
				float** a_old = A.matrix;
				A.matrix = (float**)realloc(A.matrix, ++A.m * sizeof(float**));
				A.matrix[0] = new float[A.n]{ 0 };
				for (int i = 1; i < A.m; i++) {
					A.matrix[i] = a_old[i - 1];
				}
			}
			if (naprav == 'l') {
				A.n++;
				for (int i = 0; i < A.m; i++) {
					A.matrix[i] = (float*)realloc(A.matrix[i], A.n * sizeof(float*));
					for (int j = A.n - 1; j > 0; --j) {
						A.matrix[i][j] = A.matrix[i][j - 1];
					}
					A.matrix[i][0] = 0;
				}
			}
			if (naprav == 'j') {
				A.matrix = (float**)realloc(A.matrix, ++A.m * sizeof(float**));
				A.matrix[A.m - 1] = new float[A.n]{ 0 };
			}
			break;
		}
		case 6: {
			int ch = 0, ret = 0;
			cout << "Введите значение:" << endl;
			cin >> ch;
			for (int i = 0; i < A.m; i++) {
				for (int j = 0; j < A.n; j++) {
					if (A.matrix[i][j] == ch) {
						if (ret == 0) {
							cout << "Результат: [" << i << "][" << j << "]";
							ret = ret + 1;
						}
						else
							cout << " , [" << i << "][" << j << "]";
					}
				}
			}
			if (ret == 0)
				cout << "Элемент не найден" << endl;
			cout << endl;
			break;
		}
		case 7: {
			int x = 0, y = 0;
			cout << "Введите позицию ([i][j]): " << endl;
			char ch; cin >> ch >> x >> ch >> ch >> y >> ch;
			if (x >= A.m || x < 0 || y >= A.n || y < 0) {
				cout << "Некорректная позиция" << endl;
			}
			else {
				cout << "Введите новое значение:" << endl;
				cin >> A.matrix[x][y];
			}
			break;
		}
		case 8: {
			if (A.m != A.n) {
				cout << "Матрица должна быть квадратной" << endl;
				break;
			}
			cout << "Укажите степень:" << endl;
			double st;
			cin >> st;
			if (st < 0) {
				cout << "Степень должна быть неотрицательной" << endl;
				break;
			}
			if (st - int(st) != 0) {
				cout << "Степень должна быть целым числом" << endl;
				break;
			}
			Matrix* C = pow(&A, st);
			cout << "Результат:" << endl;
			print_matrix(C);
			destroy_matrix(C);
			delete C;
			break;
		}
		case 9: {
			if (A.m != A.n) {
				cout << "Матрица должна быть квадратной" << endl;
				break;
			}
			cout << "Определитель матрицы " << determinant(&A) << endl;
			break;
		}
		case 10: {
			if (A.m != A.n) {
				cout << "Матрица должна быть квадратной" << endl;
				break;
			}
			if (determinant(&A) == 0) {
				cout << "Определитель матрицы должен быть отличен от 0" << endl;
				break;
			}
			cout << "Обратная матрица" << endl;
			Matrix* inv = inverted_matrix(&A);
			print_matrix(inv);
			destroy_matrix(inv);
			delete inv;
			break;
		}
		case 11: {
			if (A.m != A.n) {
				cout << "Матрица должна быть квадратной" << endl;
				break;
			}
			cout << "Введите степень многочлена" << endl;
			float stepen1 = 0;
			cin >> stepen1;
			if (stepen1 < 0 || (fabs(stepen1 - (int)stepen1) > 1e-5)) {
				cout << "Матрица должна быть целым положительным числом" << endl;
				break;
			}
			int stepen = (int)stepen1;
			int* arr = new int[stepen + 1]{ 0 };
			for (int i = 0; i < stepen; i++) {
				cout << "Введите коэффициент при степени " << stepen - i << endl;
				cin >> arr[i];
			}
			cout << "Введите свободный коэффициент" << endl;
			cin >> arr[stepen++];
			Matrix* C = new Matrix;
			create_matrix(C, A.m, A.n);

			for (int i = stepen - 1; i >= 0; i--) {
				Matrix* D = pow(&A, i);
				int cs = stepen - i - 1;
				for (int j = 0; j < D->m; ++j) {
					for (int k = 0; k < D->n; ++k) {
						D->matrix[j][k] *= arr[cs];
					}
				}
				Matrix* C_tmp = matrix_sum(C, D);
				destroy_matrix(C);
				delete C;
				C = C_tmp;
			}
			cout << "Результат" << endl;
			print_matrix(C);
			destroy_matrix(C);
			delete C;
			break;
		}
		case 12: {
			cout << "Введите название файла" << endl;
			string wayoffile;
			cin >> wayoffile;
			/*cin.get();
			cin.clear();*/
			ifstream wrin(wayoffile);
			bool zap;
			if (wrin.is_open()) {
				wrin.close();
				cout << "Перезаписать файл? (y/N) " << endl;
				string cho;
				do {
					cin >> cho;
					/*cin.get();
					cin.clear();*/
					if (cho == "yes" || cho == "YES" || cho == "Yes" || cho == "y" || cho == "Y")
					{
						zap = true;
						break;
					}
					else
						if (cho == "no" || cho == "NO" || cho == "No" || cho == "n" || cho == "N")
						{
							zap = false;
							break;
						}
						else
							cout << "Некорректный ввод, попробуйте еще раз" << endl;
				} while (true);
			}
			else
				zap = true;
			if (!zap)
				break;
			ofstream wrout(wayoffile);
			wrout << A.m << 'x' << A.n;
			wrout << endl;
			for (int i = 0; i < A.m; i++)
			{
				for (int j = 0; j < A.n; j++)
					wrout << setw(4) << A.matrix[i][j];
				wrout << endl;
			}
			wrout.close();
		} break;

		
		case 13: {
			cout << "Укажите путь к файлу:" << endl;
			string wayoffile;
			/*cin.get();
			cin.clear();*/
			cin >> wayoffile;
			/*cin.get();
			cin.clear();*/
			ifstream wrin(wayoffile);
			if (wrin.is_open())
			{
				destroy_matrix(&A);
				char ch;
				wrin >> A.m >> ch >> A.n;
				create_matrix(&A, A.m, A.n);
				for (int i = 0; i < A.m; i++)
					for (int j = 0; j < A.n; j++)
						wrin >> A.matrix[i][j];
			}
			else
				cout << "Не удается открыть" << endl;
			wrin.close();


		} break;
		case 14: {
			Matrix* F = new Matrix;
			create_matrix(F, A.m, A.n);
			int length;
			length = A.m*A.n;
			int*arr = new int[length];
			for (int k = 0, i = 0; i < A.m; i++)
				for (int j = 0; j < A.n; j++, k++)
					arr[k] = A.matrix[i][j];

			// Сортировка массива пузырьком
			int post;
			for (int i = 0; i < length - 1; i++) {
				for (int j = 0; j < length - i - 1; j++) {
					if (arr[j] > arr[j + 1]) {
						// меняем элементы местами
						post = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = post;
					}
				}
			}
			

			cout << "Выберите порядок сортировки: " << endl <<
				"s: snake" << endl <<
				"e: helix(snail)" << endl << 
				"a: ant" << endl;
			char cho;
			cin >> cho;
			if (cho == 's') {
				for (int z = 0, p = 0; z < length;)
				{
					for (int i = 0; i < F->m; z++, i++)
						F->matrix[i][p] = arr[z];
					p++;
					for (int i = F->m - 1; z < length && i >= 0; z++, i--)
						F->matrix[i][p] = arr[z];
					p++;
				}
			}
			if (cho == 'e') {
				for (int k = 0, cnt = 0; k < length; cnt++)
				{
					for (int i = cnt; i < F->n - cnt; k++, i++)
						F->matrix[cnt][i] = arr[k];
					for (int i = cnt + 1; k < length && i < F->m - cnt; k++, i++)
						F->matrix[i][F->n - cnt - 1] = arr[k];
					for (int i = F->n - cnt - 2; k < length && i >= cnt; i--, k++)
						F->matrix[F->m - cnt - 1][i] = arr[k];
					for (int i = F->m - cnt - 2; k < length && i>cnt; i--, k++)
						F->matrix[i][cnt] = arr[k];
				}
			}
			if (cho == 'a') {
				for (int k = 0; k < length; k++)
					F->matrix[k / F->n][k%F->n] = arr[k];
			}
			if (cho != 's' && cho != 'e' && cho != 'a') {
				cout << "Некорректный ввод" << endl;
			}
			for (int i = 0; i < F->m; i++)
			{
				for (int j = 0; j < F->n; j++)
					cout << setw(4) << F->matrix[i][j];
				cout << endl;
			}
			destroy_matrix(F);
			delete[]arr;
		} break;
		case 15:
			do {
				cout << "Вы точно хотите выйти? (Y/N):";
				string cho;
				cin >> cho;
				if ((cho == "Y") || (cho == "y") || (cho == "Yes") || (cho == "yes") || (cho == "YES")) {
					cout << "До свидания" << endl;
					destroy_matrix(&A);
					system("PAUSE");
					return 0;
				}
				if ((cho == "N") || (cho == "n") || (cho == "No") || (cho == "no") || (cho == "NO"))
					break;
				cout << "Некорректный ввод" << endl;
			} while (true);
			break;
		default:
			cout << "Операции под этим номером не существует\n";
		}

	} while (true);
}

